# To-Do List

A small to-do list app, written using MVVM in the 5 most popular languages for
developing GNOME apps.

![App Screenshot](.codeberg/screenshot.png)

## License

This project is licensed under the [GNU General Public License v3](COPYING) or
any later version.
