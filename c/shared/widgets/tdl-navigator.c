#include "tdl-navigator.h"

#include <adwaita.h>

struct _TdlNavigator
{
  GtkWidget parent_instance;
  GtkWidget *leaflet;
};

G_DEFINE_FINAL_TYPE (TdlNavigator, tdl_navigator, GTK_TYPE_WIDGET)

static void
tdl_navigator_real_dispose (GObject *object)
{
  TdlNavigator *self = TDL_NAVIGATOR (object);

  g_clear_pointer (&self->leaflet, gtk_widget_unparent);

  G_OBJECT_CLASS (tdl_navigator_parent_class)->dispose (object);
}

static void
tdl_navigator_class_init (TdlNavigatorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose  = tdl_navigator_real_dispose;

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

// Code taken and modified from
// https://gitlab.gnome.org/GNOME/libadwaita/-/blob/4ba76354bc929db9fcb43b5bdc973ff0bf07c971/src/adw-preferences-window.c#L324
static void
tdl_navigator_try_remove_view (TdlNavigator *self,
                               GParamSpec   *pspec,
                               AdwLeaflet   *leaflet)
{
  GtkWidget *child = NULL, *view = NULL;

  if (adw_leaflet_get_child_transition_running (leaflet))
    return;

  child = gtk_widget_get_first_child (GTK_WIDGET (leaflet));
  while (child != NULL)
    {
      view = child;
      child = gtk_widget_get_next_sibling (child);

      if (view == adw_leaflet_get_visible_child (leaflet))
        break;

      adw_leaflet_remove (leaflet, view);
    }
}

static void
tdl_navigator_init (TdlNavigator *self)
{
  self->leaflet = g_object_new (ADW_TYPE_LEAFLET,
                                "can-navigate-back", TRUE,
                                "can-unfold", FALSE,
                                NULL);

  gtk_widget_set_parent (self->leaflet, GTK_WIDGET (self));

  g_signal_connect_swapped (self->leaflet,
                            "notify::child-transition-running",
                            G_CALLBACK (tdl_navigator_try_remove_view),
                            self);

  g_signal_connect_swapped (self->leaflet,
                            "notify::visible-child",
                            G_CALLBACK (tdl_navigator_try_remove_view),
                            self);
}

GtkWidget *
tdl_navigator_new (void)
{
  return g_object_new (TDL_TYPE_NAVIGATOR, NULL);
}

void
tdl_navigator_push (TdlNavigator *self,
                    GtkWidget    *view)
{
  g_return_if_fail (TDL_IS_NAVIGATOR (self));
  g_return_if_fail (GTK_IS_WIDGET (view));

  if (gtk_widget_get_parent (view) != self->leaflet)
    adw_leaflet_append (ADW_LEAFLET (self->leaflet), view);

  adw_leaflet_set_visible_child (ADW_LEAFLET (self->leaflet), view);
}

void
tdl_navigator_pop (TdlNavigator *self)
{
  g_return_if_fail (TDL_IS_NAVIGATOR (self));
  adw_leaflet_navigate (ADW_LEAFLET (self->leaflet), ADW_NAVIGATION_DIRECTION_BACK);
}
