#pragma once

#include <adwaita.h>
#include "tdl-view-model.h"
#include "tdl-navigator.h"

G_BEGIN_DECLS

#define TDL_TYPE_VIEW (tdl_view_get_type())

G_DECLARE_DERIVABLE_TYPE (TdlView, tdl_view, TDL, VIEW, AdwBin)

struct _TdlViewClass
{
  AdwBinClass parent_class;
};

TdlViewModel *tdl_view_get_view_model (TdlView *self);

TdlNavigator *tdl_view_get_navigator (TdlView *self);

G_END_DECLS
