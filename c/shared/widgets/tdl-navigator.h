#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TDL_TYPE_NAVIGATOR (tdl_navigator_get_type())

G_DECLARE_FINAL_TYPE (TdlNavigator, tdl_navigator, TDL, NAVIGATOR, GtkWidget)

GtkWidget *tdl_navigator_new (void);

void tdl_navigator_push (TdlNavigator *self,
                         GtkWidget    *view);

void tdl_navigator_pop (TdlNavigator *self);

G_END_DECLS
