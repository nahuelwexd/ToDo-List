#include "tdl-view.h"

typedef struct
{
  TdlViewModel *view_model;
} TdlViewPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (TdlView, tdl_view, ADW_TYPE_BIN)

enum {
  PROP_0,
  PROP_VIEW_MODEL,
  PROP_NAVIGATOR,
  N_PROPS
};

static GParamSpec *props [N_PROPS];

static void
tdl_view_real_finalize (GObject *object)
{
  TdlView *self = TDL_VIEW (object);
  TdlViewPrivate *priv = tdl_view_get_instance_private (self);

  g_clear_object (&priv->view_model);

  G_OBJECT_CLASS (tdl_view_parent_class)->finalize (object);
}

static void
tdl_view_real_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  TdlView *self = TDL_VIEW (object);

  switch (prop_id)
    {
    case PROP_VIEW_MODEL:
      g_value_set_object (value, tdl_view_get_view_model (self));
      break;

    case PROP_NAVIGATOR:
      g_value_set_object (value, tdl_view_get_navigator (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tdl_view_real_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  TdlView *self = TDL_VIEW (object);
  TdlViewPrivate *priv = tdl_view_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_VIEW_MODEL:
      if (g_set_object (&priv->view_model, g_value_get_object (value)))
        g_object_notify_by_pspec (object, pspec);

      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tdl_view_class_init (TdlViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = tdl_view_real_finalize;
  object_class->get_property = tdl_view_real_get_property;
  object_class->set_property = tdl_view_real_set_property;

  props[PROP_VIEW_MODEL] =
    g_param_spec_object ("view-model", NULL, NULL,
                         TDL_TYPE_VIEW_MODEL,
                         G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_EXPLICIT_NOTIFY |
                           G_PARAM_STATIC_STRINGS);

  props[PROP_NAVIGATOR] =
    g_param_spec_object ("navigator", NULL, NULL,
                         TDL_TYPE_NAVIGATOR,
                         G_PARAM_READABLE |
                           G_PARAM_EXPLICIT_NOTIFY |
                           G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, props);
}

static void
tdl_view_init (TdlView *self)
{
}

TdlViewModel *
tdl_view_get_view_model (TdlView *self)
{
  g_return_val_if_fail (TDL_IS_VIEW (self), NULL);

  TdlViewPrivate *priv = tdl_view_get_instance_private (self);

  return priv->view_model;
}

TdlNavigator *
tdl_view_get_navigator (TdlView *self)
{
  GtkWidget *navigator = NULL;

  g_return_val_if_fail (TDL_IS_VIEW (self), NULL);

  navigator = gtk_widget_get_ancestor (GTK_WIDGET (self), TDL_TYPE_NAVIGATOR);
  if (navigator == NULL)
    return NULL;

  return TDL_NAVIGATOR (navigator);
}
