#pragma once

#include <glib.h>

G_BEGIN_DECLS

typedef enum _TdlViewModelState
{
  TDL_VIEW_MODEL_INITIAL,
  TDL_VIEW_MODEL_IN_PROGRESS,
  TDL_VIEW_MODEL_SUCCESS,
  TDL_VIEW_MODEL_ERROR,
} TdlViewModelState;

G_END_DECLS
