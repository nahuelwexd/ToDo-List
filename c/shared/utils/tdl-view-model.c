#include "tdl-view-model.h"
#include "tdl-enum-types.h"

typedef struct
{
  TdlViewModelState state;
} TdlViewModelPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (TdlViewModel, tdl_view_model, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_STATE,
  N_PROPS
};

static GParamSpec *props [N_PROPS];

static void
tdl_view_model_real_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  TdlViewModel *self = TDL_VIEW_MODEL (object);

  switch (prop_id)
    {
    case PROP_STATE:
      g_value_set_enum (value, tdl_view_model_get_state (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tdl_view_model_real_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  TdlViewModel *self = TDL_VIEW_MODEL (object);

  switch (prop_id)
    {
    case PROP_STATE:
      tdl_view_model_set_state (self, g_value_get_enum (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tdl_view_model_class_init (TdlViewModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = tdl_view_model_real_get_property;
  object_class->set_property = tdl_view_model_real_set_property;

  props[PROP_STATE] =
    g_param_spec_enum ("state", NULL, NULL,
                       TDL_TYPE_VIEW_MODEL_STATE,
                       TDL_VIEW_MODEL_INITIAL,
                       G_PARAM_READWRITE |
                         G_PARAM_EXPLICIT_NOTIFY |
                         G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, props);
}

static void
tdl_view_model_init (TdlViewModel *self)
{
  TdlViewModelPrivate *priv = tdl_view_model_get_instance_private (self);
  priv->state = TDL_VIEW_MODEL_INITIAL;
}

TdlViewModelState
tdl_view_model_get_state (TdlViewModel *self)
{
  g_return_val_if_fail (TDL_IS_VIEW_MODEL (self), TDL_VIEW_MODEL_INITIAL);

  TdlViewModelPrivate *priv = tdl_view_model_get_instance_private (self);

  return priv->state;
}

void
tdl_view_model_set_state (TdlViewModel      *self,
                          TdlViewModelState  value)
{
  g_return_if_fail (TDL_IS_VIEW_MODEL (self));

  TdlViewModelPrivate *priv = tdl_view_model_get_instance_private (self);

  if (value == priv->state)
    return;

  priv->state = value;

  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_STATE]);
}
