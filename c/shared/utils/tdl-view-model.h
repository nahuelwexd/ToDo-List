#pragma once

#include <glib-object.h>
#include "tdl-view-model-state.h"

G_BEGIN_DECLS

#define TDL_TYPE_VIEW_MODEL (tdl_view_model_get_type())

G_DECLARE_DERIVABLE_TYPE (TdlViewModel, tdl_view_model, TDL, VIEW_MODEL, GObject)

struct _TdlViewModelClass
{
  GObjectClass parent_class;
};

TdlViewModelState tdl_view_model_get_state (TdlViewModel      *self);
void              tdl_view_model_set_state (TdlViewModel      *self,
                                            TdlViewModelState  value);

G_END_DECLS
