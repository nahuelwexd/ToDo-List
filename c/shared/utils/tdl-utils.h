#pragma once

#include <glib.h>

G_BEGIN_DECLS

typedef gboolean (*TdlPredicateFunc) (gconstpointer obj,
                                      gpointer      user_data);

G_END_DECLS
