#include <glib.h>
#include "tdl-app.h"

gint
main (gint    argc,
      gchar **argv)
{
  g_autoptr (TdlApp) app = tdl_app_new ();
  return g_application_run (G_APPLICATION (app), argc, argv);
}
