#include "tdl-task.h"

struct _TdlTask
{
  GObject parent_instance;
  gchar *name;
  gboolean is_completed;
};

G_DEFINE_FINAL_TYPE (TdlTask, tdl_task, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_IS_COMPLETED,
  N_PROPS
};

static GParamSpec *props [N_PROPS];

static void
tdl_task_real_finalize (GObject *object)
{
  TdlTask *self = TDL_TASK (object);

  g_clear_pointer (&self->name, g_free);

  G_OBJECT_CLASS (tdl_task_parent_class)->finalize (object);
}

static void
tdl_task_real_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  TdlTask *self = TDL_TASK (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, tdl_task_get_name (self));
      break;

    case PROP_IS_COMPLETED:
      g_value_set_boolean (value, tdl_task_get_is_completed (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tdl_task_real_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  TdlTask *self = TDL_TASK (object);

  switch (prop_id)
    {
    case PROP_NAME:
      tdl_task_set_name (self, g_value_get_string (value));
      break;

    case PROP_IS_COMPLETED:
      tdl_task_set_is_completed (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tdl_task_class_init (TdlTaskClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = tdl_task_real_finalize;
  object_class->get_property = tdl_task_real_get_property;
  object_class->set_property = tdl_task_real_set_property;

  props[PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         NULL,
                         G_PARAM_READWRITE |
                           G_PARAM_EXPLICIT_NOTIFY |
                           G_PARAM_STATIC_STRINGS);

  props[PROP_IS_COMPLETED] =
    g_param_spec_boolean ("is-completed", NULL, NULL,
                          FALSE,
                          G_PARAM_READWRITE |
                            G_PARAM_EXPLICIT_NOTIFY |
                            G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, props);
}

static void
tdl_task_init (TdlTask *self)
{
}

TdlTask *
tdl_task_new (void)
{
  return g_object_new (TDL_TYPE_TASK, NULL);
}

const gchar *
tdl_task_get_name (TdlTask *self)
{
  g_return_val_if_fail (TDL_IS_TASK (self), NULL);
  return self->name;
}

void
tdl_task_set_name (TdlTask     *self,
                   const gchar *value)
{
  g_return_if_fail (TDL_IS_TASK (self));

  if (g_set_str (&self->name, value))
    g_object_notify_by_pspec (G_OBJECT (self), props[PROP_NAME]);
}

gboolean
tdl_task_get_is_completed (TdlTask *self)
{
  g_return_val_if_fail (TDL_IS_TASK (self), FALSE);
  return self->is_completed;
}

void
tdl_task_set_is_completed (TdlTask  *self,
                           gboolean  value)
{
  g_return_if_fail (TDL_IS_TASK (self));

  value = !!value;

  if (value == self->is_completed)
    return;

  self->is_completed = value;

  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_IS_COMPLETED]);
}
