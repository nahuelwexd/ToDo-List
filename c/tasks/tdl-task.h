#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define TDL_TYPE_TASK (tdl_task_get_type())

G_DECLARE_FINAL_TYPE (TdlTask, tdl_task, TDL, TASK, GObject)

TdlTask *tdl_task_new (void);

const gchar *tdl_task_get_name (TdlTask     *self);
void         tdl_task_set_name (TdlTask     *self,
                                const gchar *value);

gboolean tdl_task_get_is_completed (TdlTask  *self);
void     tdl_task_set_is_completed (TdlTask  *self,
                                    gboolean  value);

G_END_DECLS
