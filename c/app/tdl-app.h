#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define TDL_TYPE_APP (tdl_app_get_type())

G_DECLARE_FINAL_TYPE (TdlApp, tdl_app, TDL, APP, AdwApplication)

TdlApp *tdl_app_new (void) G_GNUC_WARN_UNUSED_RESULT;

G_END_DECLS
