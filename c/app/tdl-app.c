#include "tdl-app.h"

#include "tdl-app-window.h"
#include "tdl-navigator.h"

struct _TdlApp
{
  AdwApplication parent_instance;
};

G_DEFINE_FINAL_TYPE (TdlApp, tdl_app, ADW_TYPE_APPLICATION)

static void
tdl_app_real_activate (GApplication *application)
{
  TdlApp *self = TDL_APP (application);
  GtkWindow *window = NULL;

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  if (window == NULL)
    window = GTK_WINDOW (tdl_app_window_new (self));

  gtk_window_present (window);
}

static void
tdl_app_show_about_window (GSimpleAction *action,
                           GVariant      *parameter,
                           gpointer       user_data)
{
  TdlApp *self = TDL_APP (user_data);
  const gchar *developers[] = { "Nahuel Gomez Castro https://nahuelwexd.com", NULL };

  adw_show_about_window (gtk_application_get_active_window (GTK_APPLICATION (self)),
                         "application-name", "To-Do List",
                         "developer-name", "Nahuel Gomez Castro",
                         "website", "https://codeberg.org/nahuelwexd/ToDo-List",
                         "developers", developers,
                         "copyright", "© 2023 Nahuel Gomez Castro",
                         "license-type", GTK_LICENSE_GPL_3_0,
                         NULL);
}

static void
tdl_app_quit (GSimpleAction *action,
              GVariant      *parameter,
              gpointer       user_data)
{
  TdlApp *self = TDL_APP (user_data);
  g_application_quit (G_APPLICATION (self));
}

static void
tdl_app_init_actions (TdlApp *self)
{
  const GActionEntry action_entries[] = {
    { "show-about-window", tdl_app_show_about_window },
    { "quit",              tdl_app_quit              }
  };

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   action_entries,
                                   G_N_ELEMENTS (action_entries),
                                   self);

  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "app.quit",
                                         (const gchar *[]) { "<Ctrl>Q", NULL });
}

static void
tdl_app_init_widgets (void)
{
  g_type_ensure (TDL_TYPE_NAVIGATOR);
}

static void
tdl_app_real_startup (GApplication *application)
{
  TdlApp *self = TDL_APP (application);

  G_APPLICATION_CLASS (tdl_app_parent_class)->startup (application);

  g_set_application_name ("To-Do List");

  tdl_app_init_actions (self);
  tdl_app_init_widgets ();
}

static void
tdl_app_class_init (TdlAppClass *klass)
{
  GApplicationClass *application_class = G_APPLICATION_CLASS (klass);

  application_class->activate = tdl_app_real_activate;
  application_class->startup = tdl_app_real_startup;
}

static void
tdl_app_init (TdlApp *self)
{
}

TdlApp *
tdl_app_new (void)
{
  return g_object_new (TDL_TYPE_APP,
                       "application-id", "com.nahuelwexd.ToDoList.c",
                       "resource-base-path", "/com/nahuelwexd/ToDoList",
                       NULL);
}
