#pragma once

#include <adwaita.h>
#include "tdl-app.h"

G_BEGIN_DECLS

#define TDL_TYPE_APP_WINDOW (tdl_app_window_get_type())

G_DECLARE_FINAL_TYPE (TdlAppWindow, tdl_app_window, TDL, APP_WINDOW, AdwApplicationWindow)

GtkWidget *tdl_app_window_new (TdlApp *app);

G_END_DECLS
