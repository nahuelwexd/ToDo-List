#include "tdl-app-window.h"

#include "tdl-home-view.h"

struct _TdlAppWindow
{
  AdwApplicationWindow parent_instance;
  GtkWidget *navigator;
};

G_DEFINE_FINAL_TYPE (TdlAppWindow, tdl_app_window, ADW_TYPE_APPLICATION_WINDOW)

static void
tdl_app_window_real_dispose (GObject *object)
{
  TdlAppWindow *self = TDL_APP_WINDOW (object);

  gtk_widget_dispose_template (GTK_WIDGET (self), TDL_TYPE_APP_WINDOW);

  G_OBJECT_CLASS (tdl_app_window_parent_class)->dispose (object);
}

static void
tdl_app_window_class_init (TdlAppWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = tdl_app_window_real_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/com/nahuelwexd/ToDoList/ui/tdl-app-window.ui");
  gtk_widget_class_bind_template_child (widget_class, TdlAppWindow, navigator);
}

static void
tdl_app_window_init (TdlAppWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  tdl_navigator_push (TDL_NAVIGATOR (self->navigator), tdl_home_view_new ());
}

GtkWidget *
tdl_app_window_new (TdlApp *app)
{
  return g_object_new (TDL_TYPE_APP_WINDOW,
                       "application", app,
                       NULL);
}
