#pragma once

#include "tdl-view-model.h"

G_BEGIN_DECLS

#define TDL_TYPE_HOME_VIEW_MODEL (tdl_home_view_model_get_type())

G_DECLARE_FINAL_TYPE (TdlHomeViewModel, tdl_home_view_model, TDL, HOME_VIEW_MODEL, TdlViewModel)

TdlHomeViewModel *tdl_home_view_model_new (void);

G_END_DECLS
