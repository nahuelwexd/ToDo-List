#pragma once

#include "tdl-view.h"

G_BEGIN_DECLS

#define TDL_TYPE_HOME_VIEW (tdl_home_view_get_type())

G_DECLARE_FINAL_TYPE (TdlHomeView, tdl_home_view, TDL, HOME_VIEW, TdlView)

GtkWidget *tdl_home_view_new (void);

G_END_DECLS
