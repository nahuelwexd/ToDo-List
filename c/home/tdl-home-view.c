#include "tdl-home-view.h"

#include "tdl-home-view-model.h"

struct _TdlHomeView
{
  TdlView parent_instance;
};

G_DEFINE_FINAL_TYPE (TdlHomeView, tdl_home_view, TDL_TYPE_VIEW)

static void
tdl_home_view_class_init (TdlHomeViewClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  gtk_widget_class_set_template_from_resource (widget_class, "/com/nahuelwexd/ToDoList/ui/tdl-home-view.ui");
}

static void
tdl_home_view_init (TdlHomeView *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

GtkWidget *
tdl_home_view_new (void)
{
  g_autoptr (TdlHomeViewModel) view_model = tdl_home_view_model_new ();

  return g_object_new (TDL_TYPE_HOME_VIEW,
                       "view-model", view_model,
                       NULL);
}
