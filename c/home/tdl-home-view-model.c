#include "tdl-home-view-model.h"

struct _TdlHomeViewModel
{
  TdlViewModel parent_instance;
};

G_DEFINE_FINAL_TYPE (TdlHomeViewModel, tdl_home_view_model, TDL_TYPE_VIEW_MODEL)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *props [N_PROPS];

static void
tdl_home_view_model_real_finalize (GObject *object)
{
  TdlHomeViewModel *self = (TdlHomeViewModel *)object;

  G_OBJECT_CLASS (tdl_home_view_model_parent_class)->finalize (object);
}

static void
tdl_home_view_model_real_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  TdlHomeViewModel *self = TDL_HOME_VIEW_MODEL (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tdl_home_view_model_real_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  TdlHomeViewModel *self = TDL_HOME_VIEW_MODEL (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tdl_home_view_model_class_init (TdlHomeViewModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = tdl_home_view_model_real_finalize;
  object_class->get_property = tdl_home_view_model_real_get_property;
  object_class->set_property = tdl_home_view_model_real_set_property;
}

static void
tdl_home_view_model_init (TdlHomeViewModel *self)
{
}

TdlHomeViewModel *
tdl_home_view_model_new (void)
{
  return g_object_new (TDL_TYPE_HOME_VIEW_MODEL, NULL);
}
