#include "tdl-collection.h"

#include <gio/gio.h>

struct _TdlCollection
{
  GObject parent_instance;
  gchar *name;
  GPtrArray *tasks;
};

static void tdl_collection_list_model_init (GListModelInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (TdlCollection, tdl_collection, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, tdl_collection_list_model_init))

enum {
  PROP_0,
  PROP_NAME,
  N_PROPS
};

static GParamSpec *props [N_PROPS];

static void
tdl_collection_real_finalize (GObject *object)
{
  TdlCollection *self = TDL_COLLECTION (object);

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->tasks, g_ptr_array_unref);

  G_OBJECT_CLASS (tdl_collection_parent_class)->finalize (object);
}

static void
tdl_collection_real_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  TdlCollection *self = TDL_COLLECTION (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, tdl_collection_get_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tdl_collection_real_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  TdlCollection *self = TDL_COLLECTION (object);

  switch (prop_id)
    {
    case PROP_NAME:
      tdl_collection_set_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tdl_collection_class_init (TdlCollectionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = tdl_collection_real_finalize;
  object_class->get_property = tdl_collection_real_get_property;
  object_class->set_property = tdl_collection_real_set_property;

  props[PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         NULL,
                         G_PARAM_READWRITE |
                           G_PARAM_EXPLICIT_NOTIFY |
                           G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, props);
}

static void
tdl_collection_init (TdlCollection *self)
{
  self->tasks = g_ptr_array_new_full (0, g_object_unref);
}

static GType
tdl_collection_real_get_item_type (GListModel *list)
{
  return TDL_TYPE_TASK;
}

static guint
tdl_collection_real_get_n_items (GListModel *list)
{
  TdlCollection *self = TDL_COLLECTION (list);
  return self->tasks->len;
}

static gpointer
tdl_collection_real_get_item (GListModel *list,
                              guint       position)
{
  TdlCollection *self = TDL_COLLECTION (list);

  if (position >= self->tasks->len)
    return NULL;

  return g_object_ref (g_ptr_array_index (self->tasks, position));
}

static void
tdl_collection_list_model_init (GListModelInterface *iface)
{
  iface->get_item_type = tdl_collection_real_get_item_type;
  iface->get_n_items = tdl_collection_real_get_n_items;
  iface->get_item = tdl_collection_real_get_item;
}

TdlCollection *
tdl_collection_new (void)
{
  return g_object_new (TDL_TYPE_COLLECTION, NULL);
}

const gchar *
tdl_collection_get_name (TdlCollection *self)
{
  g_return_val_if_fail (TDL_IS_COLLECTION (self), NULL);
  return self->name;
}

void
tdl_collection_set_name (TdlCollection *self,
                         const gchar   *value)
{
  g_return_if_fail (TDL_IS_COLLECTION (self));

  if (g_set_str (&self->name, value))
    g_object_notify_by_pspec (G_OBJECT (self), props[PROP_NAME]);
}

void
tdl_collection_add_task (TdlCollection *self,
                         TdlTask       *task)
{
  guint position = 0;

  g_return_if_fail (TDL_IS_COLLECTION (self));
  g_return_if_fail (TDL_IS_TASK (task));

  position = self->tasks->len;

  g_ptr_array_add (self->tasks, g_object_ref (task));

  g_list_model_items_changed (G_LIST_MODEL (self), position, 0U, 1U);
}

void
tdl_collection_remove_all_tasks (TdlCollection    *self,
                                 TdlPredicateFunc  func,
                                 gpointer          user_data)
{
  guint idx = 0;

  g_return_if_fail (TDL_IS_COLLECTION (self));

  for (idx = 0; idx < self->tasks->len; idx++)
    {
      guint position = idx, removed_tasks = 0;
      TdlTask *task = g_ptr_array_index (self->tasks, idx);

      while (func (task, user_data))
        {
          g_ptr_array_remove (self->tasks, task);
          task = g_ptr_array_index (self->tasks, ++idx);
          removed_tasks = idx - position;
        }

      if (removed_tasks > 0)
        g_list_model_items_changed (G_LIST_MODEL (self), position, removed_tasks, 0U);
    }
}
