#pragma once

#include <glib-object.h>
#include "tdl-task.h"
#include "tdl-utils.h"

G_BEGIN_DECLS

#define TDL_TYPE_COLLECTION (tdl_collection_get_type())

G_DECLARE_FINAL_TYPE (TdlCollection, tdl_collection, TDL, COLLECTION, GObject)

TdlCollection *tdl_collection_new (void);

const gchar *tdl_collection_get_name (TdlCollection *self);
void         tdl_collection_set_name (TdlCollection *self,
                                      const gchar   *value);

void tdl_collection_add_task (TdlCollection *self,
                              TdlTask       *task);

void tdl_collection_remove_all_tasks (TdlCollection    *self,
                                      TdlPredicateFunc  func,
                                      gpointer          user_data);

G_END_DECLS
